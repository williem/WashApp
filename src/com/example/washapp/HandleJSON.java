package com.example.washapp;

import android.annotation.SuppressLint;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class HandleJSON {
    private String country = "country";
    private String temperature = "temperature";
    private String humidity = "humidity";
    private String windSpd = "wind";
    private String status = "status";
    private String urlString = null;

    public volatile boolean parsingComplete = true;

    public HandleJSON(String url){
        this.urlString = url;
    }

    public String getCountry(){
        return country;
    }

    public String getTemperature(){
        return temperature;
    }

    public String getHumidity(){
        return humidity;
    }

    public String getWind(){
        return windSpd;
    }

    public String getStatus(){
        return status;
    }

    @SuppressLint("WashAppAPI")
    public void readAndParseJSON(String in){
        try{
            JSONObject reader = new JSONObject(in);
            int code = reader.getInt("cod");

            if(!(code==404)) {
                JSONObject sys = reader.getJSONObject("sys");
                country = sys.getString("country");

                JSONObject main = reader.getJSONObject("main");
                temperature = main.getString("temp");
                humidity = main.getString("humidity");

                JSONObject wind = reader.getJSONObject("wind");
                windSpd = wind.getString("speed");

                JSONArray statusArr = null;
                statusArr = reader.getJSONArray("weather");
                JSONObject sts = statusArr.getJSONObject(0);
                status = sts.getString("main");
            }else{
                country = "";
                temperature = "";
                humidity = "";
                windSpd = "";
                status = "";
            }
            parsingComplete = false;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void fetchJSON(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection)url.openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.connect();

                    InputStream stream = conn.getInputStream();

                    String data = convertStreamToString(stream);

                    readAndParseJSON(data);
                    stream.close();
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    static String convertStreamToString(InputStream is){
        @SuppressWarnings("resource")
		java.util.Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext()?s.next():"";
    }
}