package com.example.washapp;

import java.text.DecimalFormat;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class WashAppPrediction extends Activity {
	private EditText txtLocation;
	private TextView txtCountry, txtTemperature, txtCondition, txtChecked, txtSuggestion;
	private ProgressDialog progressDialog;
	private static DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");
	private String mainURL = "http://api.openweathermap.org/data/2.5/weather?q=";
	private HandleJSON obj;
	SharedPreferences sharedPreferences;
	private static final String WASHAPP_PREF = "WASHAPP_PREF";
	private static final String locationKey = "locationKey";
	private static final String countryKey = "countryKey";
	private static final String conditionKey = "conditionKey";
	
    @SuppressLint("WashAppApi")
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_app_prediction);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
        txtLocation = (EditText)findViewById(R.id.editText1);
        txtCountry = (TextView)findViewById(R.id.textView5);
        txtTemperature = (TextView)findViewById(R.id.textView6);
        txtCondition = (TextView)findViewById(R.id.textView7);
        txtChecked = (TextView)findViewById(R.id.textView9);
        txtSuggestion = (TextView)findViewById(R.id.textView11);
        Button btnInfo = (Button)findViewById(R.id.button1);
        btnInfo.setOnClickListener(new getInfo());
        sharedPreferences = getSharedPreferences(WASHAPP_PREF, Context.MODE_PRIVATE);
        if(isContained()){
        	String strChecked = "";
        	strChecked+=sharedPreferences.getString(locationKey, "").toString()
        			+"\n"+sharedPreferences.getString(countryKey, "").toString()
        			+"\n"+sharedPreferences.getString(conditionKey, "").toString();
        	txtChecked.setText(strChecked);
        }else{
        	txtChecked.setText("");
        }
    }
    
    private boolean isConnectionAvailable(){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    
    class getInfo implements Button.OnClickListener{
        @Override
        public void onClick(View view){
            if(txtLocation.getText().toString().equalsIgnoreCase("")||!isConnectionAvailable()){
                Toast.makeText(getApplicationContext(), "Please check your input or connection",
                        Toast.LENGTH_SHORT).show();
            }else{
                new openWeather().execute();
            }
        }
    }
    
    public boolean isContained(){
    	sharedPreferences = getSharedPreferences(WASHAPP_PREF, Context.MODE_PRIVATE);
        return (sharedPreferences.contains(locationKey) 
        		&& sharedPreferences.contains(countryKey)
                && sharedPreferences.contains(conditionKey));               
    }
  
    private String stringSuggestion(String condition){
        String suggestion = "";
        if(condition.equalsIgnoreCase("RAIN")){
        	suggestion = "Good time to do laundry\nIt rains and there could be\n"
        			+ "more customers\nIncome could be increased until 30%";
        }else if(condition.equalsIgnoreCase("HAZE")||condition.equalsIgnoreCase("MIST")){
        	suggestion = "Nice to do laundry\nA bit misty and could be more customers\n"
        			+ "Income could be increased until 10%";
        }else if(condition.equalsIgnoreCase("CLOUDS")){
        	suggestion = "Good to do laundry\nIt's cloudy and could only be\n"
        			+ "average customers\nIncome could be same or increased until 3%";
        }else if(condition.equalsIgnoreCase("CLEAR")){
        	suggestion = "Not very good day\nIt's clear and customers could do laundry\n"
        			+ "by themselves\nIncome could be same or even decreased for 5%";
        }else{
        	suggestion = "Perfect to do laundry\nIt's snowy and there could be more customers\n"
        			+ "Remember to use heater to dry quickly\nIncome could be up to 30%";
        }
        return suggestion;
    }
    
    public void saveInfo(View view){
   		if(txtLocation.getText().toString().equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(), "Please check your input",
                   Toast.LENGTH_SHORT).show();
        }else{
        	AlertDialog.Builder alertBox = new AlertDialog.Builder(this);
        	alertBox.setMessage("Are you sure?");
        	alertBox.setPositiveButton("Yes", new DialogInterface.OnClickListener(){				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
		            sharedPreferences = getSharedPreferences(WASHAPP_PREF, Context.MODE_PRIVATE);
		            Editor editor = sharedPreferences.edit();
		            editor.putString(locationKey, txtLocation.getText().toString());
		            editor.putString(countryKey, txtCountry.getText().toString());
		            editor.putString(conditionKey, txtCondition.getText().toString());
		            editor.commit();
		            if(isContained()){
		            	String strChecked = "";
		            	strChecked+=sharedPreferences.getString(locationKey, "").toString()
		            			+"\n"+sharedPreferences.getString(countryKey, "").toString()
		            			+"\n"+sharedPreferences.getString(conditionKey, "").toString();
		            	txtChecked.setText(strChecked);
		            }
		            dialog.dismiss();
				}
			});
        	alertBox.setNegativeButton("No", new DialogInterface.OnClickListener() {				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Toast.makeText(getApplicationContext(), "No Data Saved",
							Toast.LENGTH_SHORT).show();
					dialog.dismiss();
				}
			});
        	AlertDialog alert = alertBox.create();
        	alert.show();
        }
    }
       
    private class openWeather extends AsyncTask<Void, Void, Void>{
        private boolean isNotFound(String cty, String tmp, String sts){
            return cty.equalsIgnoreCase("") || tmp.equalsIgnoreCase("") ||                    
                    sts.equalsIgnoreCase("");
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(WashAppPrediction.this);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Getting Info");
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... args){
            try{
                String url = txtLocation.getText().toString();
                String finalUrl = mainURL + url;
                obj = new HandleJSON(finalUrl);
                obj.fetchJSON();
                while(obj.parsingComplete){
                    if(isCancelled()) break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onCancelled(){
            this.cancel(true);
        }

        @Override
        protected void onPostExecute(Void args){
            if(!isNotFound(obj.getCountry(),obj.getTemperature(),obj.getStatus())){
            	progressDialog.dismiss();
                txtCountry.setText(obj.getCountry());
                double tmp = Double.parseDouble(obj.getTemperature())-273.15;
                txtTemperature.setText(REAL_FORMATTER.format(tmp)+"\u2103");              
                txtCondition.setText(obj.getStatus());
                String sugg = stringSuggestion(obj.getStatus());
                txtSuggestion.setText(sugg);
            }else{
            	progressDialog.dismiss();
                txtCountry.setText("");
                txtTemperature.setText("");           
                txtCondition.setText("");
                txtSuggestion.setText("");
                Toast.makeText(getApplicationContext(),"Location Not Found",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}