package com.example.washapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class WashAppMainPage extends Activity {
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_app_main_page);
        Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/RobotoCondensed-BoldItalic.ttf");
        TextView tv = (TextView) findViewById(R.id.txtWashApp);
        tv.setTypeface(tf);
    }

    public void goToPage(View view){
        switch(view.getId()){
            case R.id.btnWeather:
                intent = new Intent(this,WashAppWeather.class);
                break;
            case R.id.btnPrediction:
                intent = new Intent(this,WashAppPrediction.class);
                break;
            case R.id.btnTipsnTricks:
                intent = new Intent(this,WashAppTipsnTricks.class);
                break;
            case R.id.btnAboutUs:
                intent = new Intent(this,WashAppAboutUs.class);
                break;
        }
        startActivity(intent);
    }
}
