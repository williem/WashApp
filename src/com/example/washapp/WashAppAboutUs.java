package com.example.washapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.widget.TextView;

public class WashAppAboutUs extends Activity{
    @SuppressLint("WashAppApi")
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_app_about_us);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface tf = Typeface.createFromAsset(getAssets(),
                "fonts/RobotoCondensed-Bold.ttf");
        TextView tv = (TextView) findViewById(R.id.about1);
        tv.setTypeface(tf);

        Typeface tf2 = Typeface.createFromAsset(getAssets(),
                "fonts/RobotoCondensed-Italic.ttf");
        TextView tv2 = (TextView) findViewById(R.id.about2);
        tv2.setTypeface(tf2);

        Typeface tf3 = Typeface.createFromAsset(getAssets(),
                "fonts/RobotoCondensed-BoldItalic.ttf");
        TextView tv3 = (TextView) findViewById(R.id.about3);
        tv3.setTypeface(tf3);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
