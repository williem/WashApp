package com.example.washapp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class WashAppWeather extends Activity{

    private EditText location;
    private TextView country, temperature, humidity, wind, status, suggestion;
    private ImageView image;

    private String mainURL = "http://api.openweathermap.org/data/2.5/weather?q=";
    private int result = 0;
    private HandleJSON obj;
    private ProgressDialog progressDialog;
    private static DecimalFormat REAL_FORMATTER = new DecimalFormat("0.###");

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wash_app_weather);

        location = (EditText)findViewById(R.id.editText1);
        country = (TextView)findViewById(R.id.editText2);
        temperature = (TextView)findViewById(R.id.editText3);
        humidity = (TextView)findViewById(R.id.editText4);
        wind = (TextView)findViewById(R.id.editText5);
        status = (TextView)findViewById(R.id.editText6);
        suggestion = (TextView)findViewById(R.id.textView7);
        image = (ImageView)findViewById(R.id.imageView);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Button btnWeather = (Button)findViewById(R.id.button1);
        btnWeather.setOnClickListener(new getInfo());
    }

    private boolean isConnectionAvailable(){
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    
    class getInfo implements Button.OnClickListener{
        @Override
        public void onClick(View view){
            if(location.getText().toString().equalsIgnoreCase("")||!isConnectionAvailable()){
                Toast.makeText(getApplicationContext(), "Please check your input or connection",
                        Toast.LENGTH_SHORT).show();
            }else{
                new openWeather().execute();
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void imgCondition(String condition){
        if(condition.equalsIgnoreCase("CLEAR")){
            image.setImageResource(R.drawable.ic_clear);
        }else if(condition.equalsIgnoreCase("CLOUDS")){
            image.setImageResource(R.drawable.ic_cloud);
        }else if(condition.equalsIgnoreCase("RAIN")){
            image.setImageResource(R.drawable.ic_rain);
        }else if(condition.equalsIgnoreCase("HAZE")||condition.equalsIgnoreCase("MIST")){
            image.setImageResource(R.drawable.ic_mix1);
        }else{ //SNOW
            image.setImageResource(R.drawable.ic_snow);
        }
    }

    private int calculation(double temp, int hum, double wind){
        double percentage = 0.0;
        percentage = (temp*0.5) + (hum*0.3) + (wind*0.2);

        if(percentage>=0 && percentage <20)
            result = 1;
        else if(percentage>=20 && percentage <30)//re-check
            result = 2;
        else
            result = 3;

        return result;
    }

    private String stringSuggestion(int calculationResult, String condition){
        String suggestion = "";
        if(condition.equalsIgnoreCase("RAIN") || calculationResult==1)
            suggestion = "Do not do laundry";
        else if(calculationResult==2)
            suggestion = "Nice day to do laundry";
        else
            suggestion = "Perfect day to do laundry";

        return suggestion;
    }

    private class openWeather extends AsyncTask<Void, Void, Void>{
        private boolean isNotFound(String cty, String tmp, String hum, String win, String sts){
            return cty.equalsIgnoreCase("") || tmp.equalsIgnoreCase("") ||
                    hum.equalsIgnoreCase("")|| win.equalsIgnoreCase("")||
                    sts.equalsIgnoreCase("");
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            progressDialog = new ProgressDialog(WashAppWeather.this);
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Getting Info");
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... args){
            try{
                String url = location.getText().toString();
                String finalUrl = mainURL + url;
                obj = new HandleJSON(finalUrl);
                obj.fetchJSON();
                while(obj.parsingComplete){
                    if(isCancelled()) break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onCancelled(){
            this.cancel(true);
        }

        @Override
        protected void onPostExecute(Void args){
            //progressDialog.dismiss();
            if(!isNotFound(obj.getCountry(),obj.getTemperature(),obj.getHumidity(),obj.getWind(),
            obj.getStatus())){
            	progressDialog.dismiss();
                country.setText(obj.getCountry());
                double tmp = Double.parseDouble(obj.getTemperature())-273.15;
                temperature.setText(REAL_FORMATTER.format(tmp)+"\u2103");
                int humid = Integer.parseInt(obj.getHumidity());
                humidity.setText(obj.getHumidity()+"%");
                double windSpd = Double.parseDouble(obj.getWind());
                wind.setText(obj.getWind()+"m/s");
                status.setText(obj.getStatus());
                imgCondition(obj.getStatus());
                int finalCalc = calculation(tmp, humid, windSpd);
                suggestion.setText(stringSuggestion(finalCalc, obj.getStatus()));
            }else{
            	progressDialog.dismiss();
                country.setText("");
                temperature.setText("");
                humidity.setText("");
                wind.setText("");
                status.setText("");
                suggestion.setText("");
                Toast.makeText(getApplicationContext(),"Location Not Found",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}